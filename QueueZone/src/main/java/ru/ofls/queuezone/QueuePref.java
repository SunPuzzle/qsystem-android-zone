package ru.ofls.queuezone;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by 1 on 24.11.2015.
 */
@SharedPref
public interface QueuePref {
    @DefaultString("1")
    String zone();
}
