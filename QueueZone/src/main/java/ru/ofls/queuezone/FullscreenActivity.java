package ru.ofls.queuezone;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsoluteLayout;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterPreferences;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.CharBuffer;


@EActivity(R.layout.activity_fullscreen)
@WindowFeature({Window.FEATURE_NO_TITLE, Window.FEATURE_INDETERMINATE_PROGRESS})

public class FullscreenActivity extends Activity {

    @Pref
    QueuePref_ pref;

    private ServerSocket serverSocket;

    Handler updateConversationHandler;

    Thread serverThread = null;

    @ViewById(R.id.tvStatus)
    TextView text;

    public static final int SERVERPORT = 6000;

    @ViewById(R.id.frmMain)
    RelativeLayout frmMain;

    String zone="1";

    Animation anim;

    @AfterViews
    void afterViews() {
        try {
            zone=pref.zone().getOr("1");

            anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(350); //You can manage the time of the blink with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);

            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);

            File imgFile = new File("/mnt/sdcard/PICTURES/queue.png");

            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                BitmapDrawable bd = new BitmapDrawable(getResources(), myBitmap);
                frmMain.setBackground(bd);
            }

            Typeface tf = Typeface.createFromAsset(getAssets(),
                    "fonts/HeliosCondBold.ttf");

            text.setTypeface(tf);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateConversationHandler = new Handler();

        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            serverThread.interrupt();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));

                //AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Zone number");
                alert.setMessage("Input zone number");


                // Set an EditText view to get user input
                final EditText input = new EditText(this);
                alert.setView(input);
                input.setText(zone);
                input.setFilters(new InputFilter[]{
                        // Maximum 2 characters.
                        new InputFilter.LengthFilter(2),
                        // Digits only.
                        DigitsKeyListener.getInstance(),  // Not strictly needed, IMHO.
                });
                input.setKeyListener(DigitsKeyListener.getInstance());
                alert.setPositiveButton("Change",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                zone = input.getText().toString();
                                pref.clear();
                                pref.zone().put(zone);
                            }
                        });

                alert.setNegativeButton("Keep", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // do nothing
                    }
                });
                alert.show();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;
            try {
                serverSocket = new ServerSocket(SERVERPORT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted() && !serverSocket.isClosed()) {

                try {

                    socket = serverSocket.accept();

                    CommunicationThread commThread = new CommunicationThread(socket);
                    new Thread(commThread).start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;
        private BufferedWriter output;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream(),"UTF-8"));
                this.output = new BufferedWriter(new OutputStreamWriter(this.clientSocket.getOutputStream(),"UTF-8"));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        public void run() {

            while (!Thread.currentThread().isInterrupted() && clientSocket.isConnected()) {

                try {

                    CharBuffer buf=CharBuffer.allocate(4000);
                    int c=input.read(buf);
                    buf.rewind();
                    String s=URLDecoder.decode(buf.toString().trim(),"UTF-8");

                    try {
                        final JSONObject jObj=new JSONObject(s);
                        String method=jObj.getString("method");
                        int responsResilt=1;
                        if(method.equals("ping"))
                        {
                            String r="{\"result\":1,\"jsonrpc\":\"2.0\",\"id\":\""+System.currentTimeMillis()+"\"}";
                            output.write(URLEncoder.encode(r, "UTF-8"));
                            output.flush();
                            clientSocket.close();

                            updateConversationHandler.post(new updateUIThread(".", true));
                            return;
                        }else
                        if(method.equals("show"))
                        {
                            JSONObject result=jObj.getJSONObject("result");
                            String customerPrefix=result.getString("customerPrefix");
                            String userPoint=result.getString("userPoint");
                            String customerNumber=result.getString("customerNumber");
                            if(!userPoint.equals(zone))responsResilt=0; else
                            {
                                updateConversationHandler.post(new updateUIThread(customerPrefix+customerNumber,true));
                            }
                            String r="{\"result\":"+responsResilt+",\"jsonrpc\":\"2.0\",\"id\":\"" + System.currentTimeMillis()+"\"}";
                            output.write(URLEncoder.encode(r, "UTF-8"));
                            output.flush();
                            clientSocket.close();

                            return;
                        }else
                        if(method.equals("work"))
                        {
                            JSONObject result=jObj.getJSONObject("result");
                            String customerPrefix=result.getString("customerPrefix");
                            String userPoint=result.getString("userPoint");
                            String customerNumber=result.getString("customerNumber");
                            if(!userPoint.equals(zone))responsResilt=0; else
                            {
                                updateConversationHandler.post(new updateUIThread(customerPrefix+customerNumber,false));
                            }
                            String r="{\"result\":"+responsResilt+",\"jsonrpc\":\"2.0\",\"id\":\"" + System.currentTimeMillis()+"\"}";
                            output.write(URLEncoder.encode(r, "UTF-8"));
                            output.flush();
                            clientSocket.close();

                            return;
                        }else
                        if(method.equals("kill"))
                        {
                            JSONObject result=jObj.getJSONObject("result");
                            String customerPrefix=result.getString("customerPrefix");
                            String userPoint=result.getString("userPoint");
                            String customerNumber=result.getString("customerNumber");
                            if(!userPoint.equals(zone))responsResilt=0; else
                            {
                                updateConversationHandler.post(new updateUIThread("",false));
                            }
                            String r="{\"result\":"+responsResilt+",\"jsonrpc\":\"2.0\",\"id\":\"" + System.currentTimeMillis()+"\"}";
                            output.write(URLEncoder.encode(r, "UTF-8"));
                            output.flush();
                            clientSocket.close();

                            return;
                        }else{
                            JSONObject result=jObj.getJSONObject("result");
                            String customerPrefix=result.getString("customerPrefix");
                            String userPoint=result.getString("userPoint");
                            String customerNumber=result.getString("customerNumber");
                            if(!userPoint.equals(zone))responsResilt=0; else
                            {
                                updateConversationHandler.post(new updateUIThread("",false));
                            }
                            String r="{\"result\":"+responsResilt+",\"jsonrpc\":\"2.0\",\"id\":\"" + System.currentTimeMillis()+"\"}";
                            output.write(URLEncoder.encode(r, "UTF-8"));
                            output.flush();
                            clientSocket.close();

                            return;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    class updateUIThread implements Runnable {
        private String msg;
        boolean show_anim;

        public updateUIThread(String str,boolean show_anim) {
            this.msg = str;
            this.show_anim=show_anim;
        }

        @Override
        public void run() {
            if(msg!=null) {
                if(show_anim)  text.startAnimation(anim);
                else text.clearAnimation();

                text.setText(msg);
            }
        }
    }
}
